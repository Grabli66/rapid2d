package;

import rapid2d.RapTile;
import rapid2d.RapGame;

class BunnySprite extends RapTile {
    public var SpeedX : Float = 0;
    public var SpeedY : Float = 0;

    override public function OnUpdate() {
        super.OnUpdate();

        X += SpeedX;
        Y += SpeedY;
        SpeedY += 0.5;

        if (X > RapGame.Width)
        {
            SpeedX *= -1;
            X = RapGame.Width;
        }
        else if (X < 0)
        {
            SpeedX *= -1;
            X = 0;
        }
        if (Y > RapGame.Height)
        {
            SpeedY *= -0.8;
            Y = RapGame.Height;
            if (Math.random() > 0.5) SpeedY -= 3 + Math.random() * 4;
        }
        else if (Y < 0)
        {
            SpeedY = 0;
            Y = 0;
        }
    }
}
