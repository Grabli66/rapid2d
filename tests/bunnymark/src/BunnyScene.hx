package;

import rapid2d.resources.RapTileData;
import BunnySprite;
import flash.text.TextField;
import rapid2d.RapGame;
import rapid2d.RapScene;
import rapid2d.RapInput;

class BunnyScene extends RapScene {
    var _sprites : Array<BunnySprite>;
    var _texture1:RapTileData;
    var _texture2:RapTileData;

    var _text : TextField = new TextField();

    private function AddBunnies() {
        var tilesheet = RapGame.GetTilesheet(0);
        for (i in 0...100) {
            var sprite = new BunnySprite();
            var tex = _texture1;
            if (i % 2 > 0) {
                tex = _texture2;
            }
            sprite.AddAsset(tex);
            sprite.SetAsset(0);
            sprite.X = Math.random() * 100;
            sprite.Y = Math.random() * 100;
            sprite.SpeedX = Math.random() * 5;
            sprite.SpeedY = Math.random() * 5;
            tilesheet.AddTile(sprite);
            _sprites.push(sprite);
        }
    }

    override public function OnEnter() {
        var tilesheet = RapGame.GetTilesheet(0);
        _sprites = new Array<BunnySprite>();
        _texture1 = tilesheet.GetTileData("wabbit.png");
        _texture2 = tilesheet.GetTileData("wabbit2.png");
        AddBunnies();

        _text.x = 100;
        _text.y = 10;
        _text.text = 'BUNNY: ${_sprites.length}';
        RapGame.AddFlashChild(_text);
    }

    override public function OnUpdate() {
        if (RapInput.Pressed) {
            AddBunnies();
            _text.text = 'BUNNY: ${_sprites.length}';
        }
    }
}
