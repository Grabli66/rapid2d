package;

import rapid2d.RapScene;
import rapid2d.RapGame;
import rapid2d.RapInput;
import rapid2d.RapTile;

class InputScene extends RapScene {
    private var _sprite : RapTile;

    override public function OnEnter() {
        var tilesheet = RapGame.GetTilesheet(0);
        _sprite = new RapTile();
        _sprite.AddAsset(tilesheet.GetTileData("button1.png"));
        _sprite.AddAsset(tilesheet.GetTileData("button2.png"));
        _sprite.SetAsset(0);
        _sprite.X = RapGame.Width / 2 - _sprite.W / 2;
        _sprite.Y = RapGame.Height / 2 - _sprite.H / 2;
        tilesheet.AddTile(_sprite);
    }

    override public function OnUpdate() {
        if (RapInput.Pressed) {
            if (_sprite.IsHit(RapInput.X, RapInput.Y)) {
                _sprite.SetAsset(1);
            }
        } else {
            _sprite.SetAsset(0);
        }
    }
}
