package;

import rapid2d.RapGame;
import rapid2d.resources.RapPreloader;
import rapid2d.RapTilesheet;

class SnakeGame extends RapGame {
    override public function OnCreate() {
        var preloader = new RapPreloader();
        preloader.Load({
            Tilesheets : ["game"]
        }, function() {
            RapGame.AddTilesheet(new RapTilesheet(preloader.Tilesheets['game']));
            RapGame.AddScene("menu", new MenuScene());
            RapGame.AddScene("game", new GameScene());
            RapGame.SetScene("menu");
        });
    }

    override public function OnAndroidBackButton() {
        Close();
    }
}
