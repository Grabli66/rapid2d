package;

import motion.Actuate;
import rapid2d.RapTile;
import rapid2d.RapScene;
import rapid2d.RapInput;
import rapid2d.RapGame;

class MenuScene extends RapScene {
    private var _tapText : RapTile;

    override public function OnEnter() {
        var tilesheet = RapGame.GetTilesheet(0);
        var background = new RapTile();
        background.AddAsset(tilesheet.GetTileData("main_bg.png"));
        tilesheet.AddTile(background);

        var snake_label = new RapTile();
        snake_label.AddAsset(tilesheet.GetTileData("snake_label.png"));
        snake_label.X = RapGame.Width / 2 - snake_label.W / 2;
        snake_label.Y = RapGame.Height / 2 - snake_label.H / 2 - 50;
        tilesheet.AddTile(snake_label);

        var veg1 = new RapTile();
        veg1.AddAsset(tilesheet.GetTileData("veg_1.png"));
        veg1.X = 50;
        veg1.Y = RapGame.Height - veg1.H - 30;
        tilesheet.AddTile(veg1);

        var veg2 = new RapTile();
        veg2.AddAsset(tilesheet.GetTileData("veg_1.png"));
        veg2.X = RapGame.Width - veg2.W - 50;
        veg2.Y = RapGame.Height - veg2.H - 100;
        tilesheet.AddTile(veg2);

        var chick1 = new RapTile();
        chick1.AddAsset(tilesheet.GetTileData("chicken1.png"));
        chick1.X = 20;
        chick1.Y = 200;
        tilesheet.AddTile(chick1);

        var chick2 = new RapTile();
        chick2.AddAsset(tilesheet.GetTileData("chicken2.png"));
        chick2.X = RapGame.Width - chick2.W - 30;
        chick2.Y = 230;
        tilesheet.AddTile(chick2);

        _tapText = new RapTile();
        _tapText.AddAsset(tilesheet.GetTileData("tap_text.png"));
        _tapText.X = RapGame.Width / 2 - _tapText.W / 2;
        _tapText.Y = RapGame.Height - _tapText.H - 40;
        _tapText.Alpha = 0;
        tilesheet.AddTile(_tapText);

        Actuate.tween(_tapText, 1.5 , { Alpha : 1 }).repeat().reflect();
    }

    override public function OnExit() {
        Actuate.stop(_tapText);
        super.OnExit();
    }


    override public function OnUpdate() {
        if (RapInput.Pressed.Touch) {
            RapGame.SetScene("game");
        }
    }
}
