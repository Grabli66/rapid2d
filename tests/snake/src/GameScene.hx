package;

import flash.text.TextFormat;
import flash.text.TextField;
import Snake.MoveDirection;
import rapid2d.RapScene;
import rapid2d.RapGame;
import rapid2d.RapTile;
import rapid2d.RapInput;
import rapid2d.RapTimer;
import rapid2d.RapTilesheet;

class GameScene extends RapScene {
    private var _overDialog : RapTile;
    private var _snakeTimer : RapTimer;
    private var _snakeSpeedTimer : RapTimer;
    private var _chickenTimer : RapTimer;

    private var _tilesheet : RapTilesheet;
    private var _snake : Snake;
    private var _chickens = new Array<RapTile>();

    private var _scoresText : TextField;
    private var _scores : Int = 0;

    private var _isGameOver : Bool = false;

    override public function OnEnter() {
        _tilesheet = RapGame.GetTilesheet(0);
        var background = new RapTile();
        background.AddAsset(_tilesheet.GetTileData("main_bg.png"));
        _tilesheet.AddTile(background);

        var scorePlace = new RapTile();
        scorePlace.AddAsset(_tilesheet.GetTileData("score_place.png"));
        scorePlace.X = RapGame.Width - scorePlace.W + 10;
        scorePlace.Y = 10;

        var textFormat = new TextFormat();
        textFormat.color = 0xFFFFFF;
        textFormat.size = 20;
        _scoresText = new TextField();
        _scoresText.defaultTextFormat = textFormat;
        _scoresText.text = "0";
        _scoresText.x = scorePlace.X + 70;
        _scoresText.y = scorePlace.Y + 8;
        RapGame.AddFlashChild(_scoresText);

        _overDialog = new RapTile();
        _overDialog.AddAsset(_tilesheet.GetTileData("dialog.png"));
        _overDialog.X = RapGame.Width / 2 - _overDialog.W / 2;
        _overDialog.Y = RapGame.Height / 2 - _overDialog.H / 2 - 20;

        _snake = new Snake();
        _snake.OnSelfHit = function() {
            _isGameOver = true;
            _tilesheet.AddTile(_overDialog);
        }

        _snakeTimer = new RapTimer(200, function() {
            _snake.OnUpdate();
            var forDelete = new Array<RapTile>();
            for (c in _chickens) {
                if (_snake.IsHit(c)) {
                    _tilesheet.RemoveTile(c);
                    _snake.AddPart();
                    forDelete.push(c);
                    _scores += 1;
                    _scoresText.text = Std.string(_scores);
                }
            }

            for (c in forDelete) {
                _chickens.remove(c);
            }
        });

        _snakeSpeedTimer = new RapTimer(10000, function() {
            _snakeTimer.Interval -= 10;
            if (_snakeTimer.Interval < 70) _snakeTimer.Interval = 70;
        });

        _tilesheet.AddTile(scorePlace);

        _chickenTimer = new RapTimer(10000, function() {
            AddChicken();
        });
    }

    private function AddChicken() {
        var chicken = new RapTile();
        chicken.X = 100 + Math.random() * (RapGame.Width - 200);
        chicken.Y = 100 + Math.random() * (RapGame.Height - 200);

        if (chicken.X > RapGame.Width / 2) {
            chicken.AddAsset(_tilesheet.GetTileData("chicken2.png"));
        } else {
            chicken.AddAsset(_tilesheet.GetTileData("chicken1.png"));
        }
        _tilesheet.AddTile(chicken);
        _chickens.push(chicken);
    }

    override public function OnUpdate() {
        if (_isGameOver) return;

        if (RapInput.Pressed.DownKey) {
            _snake.Direction = MoveDirection.DOWN;
        }
        if (RapInput.Pressed.UpKey) {
            _snake.Direction = MoveDirection.UP;
        }
        if (RapInput.Pressed.LeftKey) {
            _snake.Direction = MoveDirection.LEFT;
        }
        if (RapInput.Pressed.RightKey) {
            _snake.Direction = MoveDirection.RIGHT;
        }

        _snakeTimer.OnUpdate();
        _chickenTimer.OnUpdate();
        _snakeSpeedTimer.OnUpdate();
    }
}
