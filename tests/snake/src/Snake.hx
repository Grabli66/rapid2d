package;

import flash.geom.Rectangle;
import rapid2d.RapTile;
import rapid2d.RapGame;
import rapid2d.RapTilesheet;

enum MoveDirection {
    UP;
    DOWN;
    LEFT;
    RIGHT;
}

class Snake {
    private var _head : RapTile;
    private var _tail : RapTile;
    private var _bodyParts = new Array<RapTile>();
    private var _speed : Int = 43;
    private var _tilesheet : RapTilesheet;

    public var Direction : MoveDirection = MoveDirection.RIGHT;
    public var OnSelfHit : Void -> Void;

    public function new() {
        _tilesheet = RapGame.GetTilesheet(0);
        _head = new RapTile();
        _head.AddAsset(_tilesheet.GetTileData("head_right.png"));
        _head.AddAsset(_tilesheet.GetTileData("head_down.png"));
        _head.AddAsset(_tilesheet.GetTileData("head_left.png"));
        _head.AddAsset(_tilesheet.GetTileData("head_up.png"));
        _head.X = RapGame.Width / 2;
        _head.Y = RapGame.Height / 2;
        _tilesheet.AddTile(_head);
        _tail = new RapTile();
        _tail.AddAsset(_tilesheet.GetTileData("tail_right.png"));
        _tail.AddAsset(_tilesheet.GetTileData("tail_down.png"));
        _tail.AddAsset(_tilesheet.GetTileData("tail_left.png"));
        _tail.AddAsset(_tilesheet.GetTileData("tail_up.png"));
        AddPart();
        AddPart();
        AddPart();
        _tilesheet.AddTile(_tail);
    }

    public function AddPart() {
        var part = new RapTile();
        if (_bodyParts.length > 0) {
            var last = _bodyParts[_bodyParts.length-1];
            part.X = last.X;
            part.Y = last.Y;
        } else {
            part.X = _head.X - 40;
            part.Y = _head.Y;
        }

        part.AddAsset(_tilesheet.GetTileData("body.png"));
        _bodyParts.push(part);
        _tilesheet.AddTile(part);
    }

    public function RemovePart() {

    }

    public function IsHit(tile : RapTile) : Bool {
        var r1 = new Rectangle(_head.X, _head.Y, _head.W, _head.H);
        var r2 = new Rectangle(tile.X, tile.Y, tile.W, tile.H);
        if (r1.intersects(r2)) return true;
        return false;
    }

    public function OnUpdate() {
        var lX = _head.X;
        var lY = _head.Y;

        switch(Direction) {
            case MoveDirection.UP: {
                _head.Y -= _speed;
                _head.SetAsset(3);
            }
            case MoveDirection.DOWN: {
                _head.Y += _speed;
                _head.SetAsset(1);
            }
            case MoveDirection.LEFT: {
                _head.X -= _speed;
                _head.SetAsset(2);
            }
            case MoveDirection.RIGHT: {
                _head.X += _speed;
                _head.SetAsset(0);
            }
        }

        for (p in _bodyParts) {
            // Checks self hits
            if (IsHit(p)) {
                OnSelfHit();
            }

            var llX = p.X;
            var llY = p.Y;
            p.X = lX;
            p.Y = lY;
            lX = llX;
            lY = llY;
        }

        if (lX > _tail.X) _tail.SetAsset(0);
        if (lX < _tail.X) _tail.SetAsset(2);
        if (lY < _tail.Y) _tail.SetAsset(3);
        if (lY > _tail.Y) _tail.SetAsset(1);

        _tail.X = lX;
        _tail.Y = lY;

        if (_head.X > RapGame.Width) _head.X = 0;
        if (_head.X < 0) _head.X = RapGame.Width;
        if (_head.Y > RapGame.Height) _head.Y = 0;
        if (_head.Y < 0) _head.Y = RapGame.Height;

    }
}
