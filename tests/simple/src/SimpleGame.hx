package;

import rapid2d.resources.RapPreloader;
import rapid2d.RapGame;
import rapid2d.RapTilesheet;
import rapid2d.RapTile;

class SimpleGame extends RapGame {

    override public function OnCreate() {
        var preloader = new RapPreloader();
        preloader.CompleteDelay = 0;

        preloader.Load({
            Tilesheets : ["textures"]
        }, function() {
            var tileSheet:RapTilesheet = new RapTilesheet(preloader.Tilesheets['textures']);
            RapGame.AddTilesheet(tileSheet);

            ShowFps = true;
            var sprite = new RapTile();
            sprite.AddAsset(tileSheet.GetTileData("wabbit.png"));
            sprite.SetAsset(0);
            sprite.X = RapGame.Width / 2;
            sprite.Y = RapGame.Height / 2;
            tileSheet.AddTile(sprite);
        });
    }

    override public function OnAndroidBackButton() {
        Close();
    }
}
