package;

import rapid2d.RapScene;
import rapid2d.RapEmitter;
import rapid2d.RapGame;

class ParticlesScene extends RapScene {
    private var _emitter:RapEmitter;

    override public function OnEnter() {
        var tilesheet = RapGame.GetTilesheet(0);
        _emitter = new RapEmitter(tilesheet);
        _emitter.Texture = tilesheet.GetTileData("smoke1.png");
        _emitter.X = RapGame.Width / 2;
        _emitter.Y = RapGame.Height / 2;
        _emitter.LifeTime = 100;
        _emitter.Speed = 2;
        _emitter.Count = 100;
    }

    override public function OnUpdate() {
        super.OnUpdate();
        _emitter.OnUpdate();
    }
}
