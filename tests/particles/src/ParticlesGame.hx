package;

import rapid2d.resources.RapPreloader;
import rapid2d.RapGame;
import rapid2d.RapTilesheet;

class ParticlesGame extends RapGame {
    override public function OnCreate() {
        ShowFps = true;

        var preloader = new RapPreloader();
        preloader.Load({
            Tilesheets : ["textures"]
        }, function() {
            RapGame.AddTilesheet(new RapTilesheet(preloader.Tilesheets['textures']));
            AddScene("particles", new ParticlesScene());
            SetScene("particles");
        });
    }

    override public function OnAndroidBackButton() {
        Close();
    }
}
