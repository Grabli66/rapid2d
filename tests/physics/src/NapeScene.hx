package;

import nape.phys.Body;
import nape.phys.BodyType;
import nape.shape.Polygon;
import nape.geom.Vec2;
import nape.space.Space;
import rapid2d.RapScene;
import rapid2d.RapGame;

class NapeScene extends RapScene {
    private var _space:Space;

    override public function OnEnter() {
        var tilesheet = RapGame.GetTilesheet(0);

        var gravity = Vec2.weak(0, 900);
        _space = new Space(gravity);

        var floor = new Body(BodyType.STATIC);
        floor.shapes.add(new Polygon(Polygon.rect(0, (RapGame.Height - 50), (RapGame.Width), 1)));
        floor.space = _space;

        var wall1 = new Body(BodyType.STATIC);
        wall1.shapes.add(new Polygon(Polygon.rect(0, 0, 10, RapGame.Height)));
        wall1.space = _space;

        var wall2 = new Body(BodyType.STATIC);
        wall2.shapes.add(new Polygon(Polygon.rect(RapGame.Width - 10, 0, 10, RapGame.Height)));
        wall2.space = _space;

        for (i in 0...100) {
            var sprite = new NapeSprite(_space);
            tilesheet.AddTile(sprite);
        }

    }

    override public function OnUpdate() {
        _space.step(1 / RapGame.FrameRate);
    }
}
