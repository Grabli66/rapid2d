package;

import rapid2d.resources.RapPreloader;
import rapid2d.RapGame;
import rapid2d.RapTilesheet;

class NapeGame extends RapGame {
    override public function OnCreate() {
        ShowFps = true;

        var preloader = new RapPreloader();
        preloader.Load({
            Tilesheets : ["textures"]
        }, function() {
            RapGame.AddTilesheet(new RapTilesheet(preloader.Tilesheets['textures']));
            AddScene("nape", new NapeScene());
            SetScene("nape");
        });
    }

    override public function OnAndroidBackButton() {
        Close();
    }
}
