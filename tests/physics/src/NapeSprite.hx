package;

import nape.phys.Material;
import nape.space.Space;
import nape.shape.Polygon;
import nape.phys.BodyType;
import nape.phys.Body;
import rapid2d.RapTile;
import rapid2d.RapGame;

class NapeSprite extends RapTile {
    private var _box : Body;

    public function new(space:Space) {
        super();
        var tilesheet = RapGame.GetTilesheet(0);
        _box = new Body(BodyType.DYNAMIC);
        _box.shapes.add(new Polygon(Polygon.box(26, 37)));
        _box.position.setxy(Math.random() * (RapGame.Width - 200) + 100, 100 * Math.random());
        _box.setShapeMaterials(new Material(0.7));
        _box.space = space;
        AddAsset(tilesheet.GetTileData("wabbit.png"));
        SetAsset(0);
    }

    override public function OnUpdate() {
        super.OnUpdate();
        X = _box.position.x;
        Y = _box.position.y;
    }
}
