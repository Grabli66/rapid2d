package;

import rapid2d.resources.RapPreloader;
import motion.Actuate;
import rapid2d.RapGame;
import rapid2d.RapTile;
import rapid2d.RapTilesheet;

class TweenGame extends RapGame {

    override public function OnCreate() {
        ShowFps = true;

        var preloader = new RapPreloader();
        preloader.Load({
            Tilesheets : ["textures"]
        }, function() {
            var tilesheet = new RapTilesheet(preloader.Tilesheets['textures']);
            RapGame.AddTilesheet(tilesheet);

            for (i in 0...100) {
                var sprite = new RapTile();
                sprite.AddAsset(tilesheet.GetTileData("wabbit.png"));
                sprite.SetAsset(0);
                sprite.X = Math.random() * RapGame.Width;
                sprite.Y = Math.random() * RapGame.Height;
                tilesheet.AddTile(sprite);
                sprite.Alpha = 1;
                Actuate.tween(sprite, 3, { X: Math.random() * RapGame.Width }).repeat().reflect();
            }
        });
    }

    override public function OnAndroidBackButton() {
        Close();
    }
}
