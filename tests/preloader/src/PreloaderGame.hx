package;

import rapid2d.resources.RapPreloader;
import rapid2d.RapGame;
import rapid2d.RapTile;
import rapid2d.RapTilesheet;

class PreloaderGame extends RapGame {

    override public function OnCreate() {
        var preloader = new RapPreloader();
        preloader.CompleteDelay = 3000;

        preloader.Load({
            Tilesheets : ["textures"],
            Sounds : ["music.ogg"]
        }, function() {
            var tilesheet = new RapTilesheet(preloader.Tilesheets['textures']);
            RapGame.AddTilesheet(tilesheet);

            ShowFps = true;
            var sprite = new RapTile();
            sprite.AddAsset(tilesheet.GetTileData("wabbit.png"));
            sprite.SetAsset(0);
            sprite.X = 100;
            sprite.Y = 100;
            tilesheet.AddTile(sprite);
        });
    }

    override public function OnAndroidBackButton() {
        Close();
    }
}
