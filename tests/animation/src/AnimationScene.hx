package;

import rapid2d.RapTile;
import rapid2d.RapScene;
import rapid2d.RapAnimation;
import rapid2d.RapGame;

class AnimationScene extends RapScene {
    override public function OnEnter() {
        var tilesheet = RapGame.GetTilesheet(0);

        var animation = new RapAnimation();
        animation.AddFrame(1);
        animation.AddFrame(2);
        animation.AddFrame(3);
        animation.AddFrame(4);
        animation.AddFrame(5);
        animation.AddFrame(6);
        animation.Speed = 100;
        animation.Looped = true;

        for (i in 0...100) {
            var golem = new RapTile();
            golem.X = Math.random() * RapGame.Width;
            golem.Y = Math.random() * RapGame.Height;
            golem.Scale = Math.random() / 0.6 + 0.3;
            golem.AddAsset(tilesheet.GetTileData("walk1.png"));
            golem.AddAsset(tilesheet.GetTileData("walk2.png"));
            golem.AddAsset(tilesheet.GetTileData("walk3.png"));
            golem.AddAsset(tilesheet.GetTileData("walk4.png"));
            golem.AddAsset(tilesheet.GetTileData("walk5.png"));
            golem.AddAsset(tilesheet.GetTileData("walk6.png"));
            golem.AddAsset(tilesheet.GetTileData("walk7.png"));
            golem.AddAnimation("walkLeft", animation);
            golem.PlayAnimation("walkLeft");
            tilesheet.AddTile(golem);
        }
    }

    override public function OnUpdate() {
    }
}
