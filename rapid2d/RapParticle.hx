package rapid2d;
class RapParticle extends RapTile {
    public var LifeTime : Float = 10;
    public var SpeedX : Float = 0;
    public var SpeedY : Float = 0;
    public var OnDead : RapParticle -> Void;
    public var ElapsedLife : Float = 0;

    override public function OnUpdate() {
        X += SpeedX;
        Y += SpeedY;

        if (ElapsedLife >= LifeTime) {
            OnDead(this);
            return;
        }
        ElapsedLife += 1;
        Alpha = 1 - (ElapsedLife / LifeTime);
    }
}
