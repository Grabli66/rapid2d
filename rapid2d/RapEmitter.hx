package rapid2d;

import rapid2d.resources.RapTileData;
class RapEmitter {
    public var X : Float = 0;
    public var Y : Float = 0;

    public var LifeTime(default,default) : Int = 10;
    public var Speed(default,default) : Float = 2;
    public var Count(default,default) : Int = 10;
    public var Texture:RapTileData;

    private var _tilesheet : RapTilesheet;
    private var _deads = new Array<RapParticle>();

    public function new(tilesheet : RapTilesheet) {
        _tilesheet = tilesheet;
    }

    public function OnUpdate() {
        var len = _deads.length;
        for (i in 0...len) {
            _tilesheet.RemoveTile(_deads[i]);
        }

        if (len > 0) _deads = new Array<RapParticle>();

        for (i in 0...Count) {
            var particle = new RapParticle();
            particle.AddAsset(Texture);
            particle.SetAsset(0);
            particle.LifeTime = LifeTime;
            particle.X = X + Math.random() * 100;
            particle.Y = Y + Math.random() * 1;

            var xDir = Math.random() > 0.5 ? 1 : -1;
            //var yDir = Math.random() > 0.5 ? 1 : -1;
            var yDir = -1;

            particle.SpeedX = Math.random() * Speed * xDir;
            particle.SpeedY = Math.random() * Speed * yDir;
            particle.OnDead = function(p: RapParticle) {
                _deads.push(p);
            };
            _tilesheet.AddTile(particle);
        }
    }

}
