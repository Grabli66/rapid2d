package rapid2d;

class RapVisual {
    public var X : Float = 0;
    public var Y : Float = 0;
    public var W : Float;
    public var H : Float;
    public var IsCentered : Bool = false;
    public var Scale : Float = 1;
    public var Alpha : Float = 1;
    public var Rotation : Float = 0;

    public function OnUpdate() {}

    public function IsHit(x : Float, y : Float) : Bool {
        if ((x >= X) && (x <= X + W) && (y > Y) && (y < Y + H)) return true;
        return false;
    }
}
