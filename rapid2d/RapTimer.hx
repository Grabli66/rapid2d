package rapid2d;

class RapTimer {
    private var _lastTime : Float = 0;
    private var _callback : Void -> Void;

    public var Interval : Int;

    public function new(interval : Int, callback : Void -> Void) {
        Interval = interval;
        _callback = callback;
    }

    public function OnUpdate() {
        var delta = RapGame.Now - _lastTime;
        if (delta >= Interval) {
            _callback();
            _lastTime = RapGame.Now;
        }
    }
}
