package rapid2d;

import flash.events.KeyboardEvent;
import flash.Lib;
import flash.events.TouchEvent;
import flash.events.MouseEvent;

class InputStates {
    public var Touch : Bool = false;
    public var UpKey : Bool = false;
    public var DownKey : Bool = false;
    public var LeftKey : Bool = false;
    public var RightKey : Bool = false;

    public function new() {}

    public function Reset() {
        Touch = false;
        UpKey = false;
        DownKey = false;
        LeftKey = false;
        RightKey = false;
    }
}

class RapInput {
    public static var Pressed = new InputStates();

    private static inline var UP_KEY_CODE = 38;
    private static inline var DOWN_KEY_CODE = 40;
    private static inline var LEFT_KEY_CODE = 37;
    private static inline var RIGHT_KEY_CODE = 39;

    public static var X : Float = 0;
    public static var Y : Float = 0;

    public static function Setup() {
        #if !mobile
            Lib.current.stage.addEventListener(MouseEvent.MOUSE_DOWN, OnMouseDown);
            Lib.current.stage.addEventListener(MouseEvent.MOUSE_UP, OnMouseUp);

            Lib.current.stage.addEventListener(KeyboardEvent.KEY_DOWN, OnKeyDown);
            Lib.current.stage.addEventListener(KeyboardEvent.KEY_UP, OnKeyUp);
        #else
            Lib.current.stage.addEventListener(TouchEvent.TOUCH_BEGIN, OnTouchStart);
            Lib.current.stage.addEventListener(TouchEvent.TOUCH_END, OnTouchEnd);
        #end
    }

    private static function OnMouseDown(e:MouseEvent) {
        X = e.stageX;
        Y = e.stageY;
        Pressed.Touch = true;
    }

    private static function OnMouseUp(e:MouseEvent) {
        Pressed.Touch = false;
    }

    private static function OnTouchStart(e:TouchEvent) {
        X = e.stageX;
        Y = e.stageY;
        Pressed.Touch = true;
    }

    private static function OnTouchEnd(e:TouchEvent) {
        Pressed.Touch = false;
    }

    private static function OnKeyDown(e : KeyboardEvent) {
        if (e.keyCode == UP_KEY_CODE) Pressed.UpKey = true;
        if (e.keyCode == DOWN_KEY_CODE) Pressed.DownKey = true;
        if (e.keyCode == LEFT_KEY_CODE) Pressed.LeftKey = true;
        if (e.keyCode == RIGHT_KEY_CODE) Pressed.RightKey = true;
    }

    private static function OnKeyUp(e : KeyboardEvent) {
        trace(e.keyCode);
        if (e.keyCode == UP_KEY_CODE) Pressed.UpKey = false;
        if (e.keyCode == DOWN_KEY_CODE) Pressed.DownKey = false;
        if (e.keyCode == LEFT_KEY_CODE) Pressed.LeftKey = false;
        if (e.keyCode == RIGHT_KEY_CODE) Pressed.RightKey = false;
    }
}
