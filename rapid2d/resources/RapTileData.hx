package rapid2d.resources;

typedef RapTileData = {
    TileIndex:Int,
    X:Float,
    Y:Float,
    W:Float,
    H:Float
}
