package rapid2d.resources;

import flash.display.BitmapData;

typedef RapTilesheetData = {
    Atlas: String,
    BitmapData: BitmapData
}
