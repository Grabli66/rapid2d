package rapid2d.resources;

import flash.events.TimerEvent;
import flash.utils.Timer;
import flash.display.BitmapData;
import openfl.Assets;
import flash.Lib;
import flash.display.Sprite;

typedef PreloaderResources = {
    ?Tilesheets:Array<String>,
    ?Sounds:Array<String>
}

class RapPreloader extends Sprite {
    private var _onComplete:Void -> Void;

    public var Loaded:Int = 0;
    public var Total:Int = 0;
    public var CompleteDelay:Int = 0;
    public var Tilesheets = new Map<String, RapTilesheetData>();
    public var Images = new Map<String, BitmapData>();

    public function new() {
        super();
        Init();
    }

    public function Load(res:PreloaderResources, onComplete:Void -> Void) {
        _onComplete = onComplete;

        Total += res.Tilesheets.length;

        for (s in res.Tilesheets) {
            Tilesheets[s] = {
                Atlas : null,
                BitmapData : null
            };

#if emulator
            Assets.loadBytes('assets/${s}.png', function(bytes) {
                Tilesheets[s].BitmapData = BitmapData.fromBytes(bytes);
                _OnUpdate('atlas.bitmap', s);
            });
#else
            Assets.loadBitmapData('assets/${s}.png', function(d:BitmapData) {
                Tilesheets[s].BitmapData = d;
                _OnUpdate('atlas.bitmap', s);
            });
#end

            Assets.loadText('assets/${s}.atlas', function(d:String) {
                Tilesheets[s].Atlas = d;
                _OnUpdate('atlas.text', s);
            });
        }
    }

    private function _OnUpdate(tp:String, name:String):Void {
        if (tp.split('.')[0] == 'atlas') {
            var tsd = Tilesheets[name];
            if ((tsd.Atlas != null) && (tsd.BitmapData != null)) {
                Loaded += 1;
            }
        }

        OnUpdate(tp, name);

        if (Loaded >= Total) {
            if (CompleteDelay > 0) {
                var timer = new Timer(CompleteDelay);
                timer.addEventListener(TimerEvent.TIMER, function(e) {
                    _onComplete();
                    flash.Lib.current.stage.removeChild(this);
                    timer.stop();
                });
                timer.start();
            } else {
                _onComplete();
                flash.Lib.current.stage.removeChild(this);
            }
        }
    }

// For override

    public function Init() {
        var color = 0x000000;
        var stg = flash.Lib.current.stage;
        graphics.beginFill(color, 1);
        graphics.drawRect(0, 0, stg.stageWidth, stg.stageHeight);
        stg.addChild(this);
    }

// For override

    public function OnUpdate(tp:String, name:String):Void {
        var stg = flash.Lib.current.stage;
        graphics.beginFill(0x33AA00);
        var wid = ((stg.stageWidth - 20) / Total) * Loaded;
        graphics.drawRect(10, stg.stageHeight - 70, wid, 30);
    }
}
