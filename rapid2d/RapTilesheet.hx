package rapid2d;

import flash.geom.Rectangle;
import rapid2d.resources.RapTilesheetData;
import flash.display.Graphics;
import rapid2d.resources.RapTileData;
import openfl.display.Tilesheet;

class RapTilesheet {
    private var _tda = new Array<Float>();
    private var _tileSheet:Tilesheet;
    private var _tilesData = new Map<String, RapTileData>();

    private var _tiles = new Array<RapTile>();
    private var _tileLen:Int = 0;

    public function new(tilesheetData : RapTilesheetData) {
        _tileSheet = new Tilesheet(tilesheetData.BitmapData);

        var strings = tilesheetData.Atlas.split("\n");
        var index = 0;
        for(i in 0...strings.length) {
            if (i == 0) continue;
            var s = strings[i];
            var aDat = s.split("\t");
            if (aDat.length < 8) continue;
            var x:Int = Std.parseInt(aDat[1]);
            var y:Int = Std.parseInt(aDat[2]);
            var w:Int = Std.parseInt(aDat[7]);
            var h:Int = Std.parseInt(aDat[8]);

            _tileSheet.addTileRect(new Rectangle (x, y, w, h));
            var tres:RapTileData = {
                X : x,
                Y : y,
                W : w,
                H : h,
                TileIndex : index
            };
            _tilesData[aDat[0]] = tres;
            index += 1;
        }
    }

    public function GetTileData(id : String) : RapTileData {
        return _tilesData[id];
    }

    public function ClearVisuals() {
        _tiles = new Array<RapTile>();
        _tda = new Array<Float>();
        _tileLen = 0;
    }

    public function AddTile(s:RapTile) {
        _tiles[_tileLen] = s;
        s.Index = _tileLen;
        _tileLen += 1;
    }

    public function RemoveTile(s:RapTile) {
        _tda = new Array<Float>();
        _tiles.remove(s);
        _tileLen -= 1;
    }

    public function Draw(g : Graphics) {
        if (_tileLen < 1) return;

        for (i in 0..._tileLen) {
            var tl = _tiles[i];
            var index = i * 6;
            tl.OnUpdate();
            if (tl.IsCentered) {
                _tda[index] = tl.X - tl.W / 2;
                _tda[index + 1] = tl.Y - tl.H / 2;
            } else {
                _tda[index] = tl.X;
                _tda[index + 1] = tl.Y;
            }
            _tda[index + 2] = tl.TileIndex;
            _tda[index + 3] = tl.Scale;
            _tda[index + 4] = tl.Rotation;
            _tda[index + 5] = tl.Alpha;
        }

        _tileSheet.drawTiles(g, _tda, true, Tilesheet.TILE_SCALE | Tilesheet.TILE_ROTATION | Tilesheet.TILE_ALPHA);
    }
}
