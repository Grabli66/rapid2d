package rapid2d;

import rapid2d.resources.RapTileData;
class RapTile extends RapVisual {
    private var _assets = new Array<RapTileData>();
    private var _animations = new Map<String, RapAnimation>();
    private var _currentAnimation:RapAnimation;
    private var _lastTime:Float = 0;
    private var _animIndex:Int = -1;

    public var Index:Int = 0;
    public var TileIndex:Int = -1;
    public var Animated:Bool = false;

    public function new() {
    }

    public function AddAsset(asset:RapTileData) {
        _assets.push(asset);
        if (_assets.length == 1) {
            SetAsset(0);
        }
    }

    public function SetAsset(index:Int) {
        var asset = _assets[index];
        W = asset.W;
        H = asset.H;
        TileIndex = asset.TileIndex;
    }

    private function NextAnimation() {
        _animIndex += 1;
        if (_animIndex >= _currentAnimation.FrameCount) {
            _animIndex = 0;
            if (!_currentAnimation.Looped) StopAnimation();
        }
    }

    public function AddAnimation(id:String, anim:RapAnimation) {
        _animations[id] = anim;
        Animated = true;
    }

    public function PlayAnimation(id:String) {
        _currentAnimation = _animations[id];
    }

    public function StopAnimation() {
        _currentAnimation = null;
        SetAsset(0);
    }

    public function ClearAnimations() {
        SetAsset(0);
    }

    private function Animate() {
        if (_currentAnimation != null) {
            if (RapGame.Now - _lastTime > _currentAnimation.Speed) {
                NextAnimation();
                SetAsset(_animIndex);
                _lastTime = RapGame.Now;
            }
        }
    }

    override public function OnUpdate() {
        if (Animated) Animate();
    }
}
