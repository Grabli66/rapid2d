package rapid2d;

import flash.display.DisplayObject;
import openfl.display.FPS;
import flash.display.StageQuality;
import flash.system.System;
import flash.events.KeyboardEvent;
import flash.events.Event;
import flash.Lib;
import flash.display.Sprite;
import flash.system.Capabilities;

class RapGame extends Sprite {
    private static inline var ANDROID_BACKBUTTON = 1073742094;
    private static inline var FPS_DELAY = 20;

    private static var _game : RapGame;
    private static var _tileSheets = new Array<RapTilesheet>();

    private var _lastTime : Float = 0;
    private static var _currentScene: RapScene;
    private static var _scenes = new Map<String, RapScene>();

    public static var Now : Float;
    public static var FrameRate : Float;
    public static var ScreenDensity (default, null):Float;
    public static var Width (default, null):Int;
    public static var Height (default, null):Int;

    public var ShowFps:Bool = false;

    public function new() {
        super();

        if (stage != null) {
            Init(null);
        }
        else {
            addEventListener(Event.ADDED_TO_STAGE, Init);
        }
        _game = this;
    }

    public static function GetTilesheet(index : Int) : RapTilesheet {
        return _tileSheets[index];
    }

    public static function AddFlashChild(s : DisplayObject) {
        _game.addChild(s);
    }

    public static function Clear() {
        for (t in _tileSheets) {
            t.ClearVisuals();
        }
    }

    public static function AddScene(id:String, scene:RapScene) {
        _scenes[id] = scene;
    }

    public static function SetScene(id:String) {
        if (_currentScene != null) _currentScene.OnExit();
        _currentScene = _scenes[id];
        _currentScene.OnEnter();
    }

    public static function AddTilesheet(tileSheet:RapTilesheet)
    {
        _tileSheets.push(tileSheet);
    }

    private function EnvSetup():Void {
#if html5
        ScreenDensity = 1;
#else
        var dpi = Capabilities.screenDPI;
        if (dpi < 200) {
            ScreenDensity = 1;

        } else if (dpi < 300) {
            ScreenDensity = 1.5;

        } else {
            ScreenDensity = 2;
        }
#end
        OnResize(null);
        Lib.current.stage.addEventListener(Event.RESIZE, OnResize);
    }

    private function Init(e:Event) {
        EnvSetup();
        RapInput.Setup();
        Lib.current.stage.quality = StageQuality.LOW;
        OnCreate();

        if (ShowFps) {
            var fps = new FPS ();
            addChild (fps);
        }

        addEventListener(Event.ENTER_FRAME, OnEnterFrame);
        Lib.current.stage.addEventListener(KeyboardEvent.KEY_UP, OnKeyUp);
    }

    private function OnResize(e:Event) {
        Width = Math.ceil(Lib.current.stage.stageWidth / ScreenDensity);
        Height = Math.ceil(Lib.current.stage.stageHeight / ScreenDensity);
    }


    private function OnEnterFrame(e:Event) {
        Now = Lib.getTimer();
        FrameRate = stage.frameRate;
        if ( Now - _lastTime < FPS_DELAY ) return;
        _lastTime = Now;

        if (_currentScene != null) _currentScene.OnUpdate();
        graphics.clear();
        for (t in _tileSheets) {
            t.Draw(graphics);
        }
    }

    private function OnKeyUp(event:KeyboardEvent):Void {
        #if android
            if (event.keyCode == ANDROID_BACKBUTTON) {
                event.stopImmediatePropagation();
                OnAndroidBackButton();
            }
        #end
    }

    public function OnCreate() {}
    public function OnAndroidBackButton() {}

    public function Close() {
        System.exit(0);
    }

    public function Run() {
        Lib.current.stage.align = flash.display.StageAlign.TOP_LEFT;
        Lib.current.stage.scaleMode = flash.display.StageScaleMode.NO_SCALE;
        Lib.current.addChild(this);
    }
}
